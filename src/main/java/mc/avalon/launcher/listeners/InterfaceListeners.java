package mc.avalon.launcher.listeners;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JFrame;

import com.neziaa.swinger.event.SwingerEvent;
import com.neziaa.swinger.event.SwingerEventListener;

import mc.avalon.launcher.Launcher;
import mc.avalon.launcher.interfaces.Interface;

public class InterfaceListeners implements SwingerEventListener {

	@SuppressWarnings("deprecation")
	@Override
	public void onEvent(SwingerEvent event) {
		
		// Browser LINKS
		if(event.getSource() == Interface.getInstance().getYoutube())
			openLink("https://www.youtube.com/c/AvalonNetwork");
		
		if(event.getSource() == Interface.getInstance().getTwitter())
			openLink("https://twitter.com/AvalonNetworkFR");
		
		if(event.getSource() == Interface.getInstance().getDiscord())
			openLink("https://discord.me/AvalonNetwork");
		
		if(event.getSource() == Interface.getInstance().getTeamspeak())
			openLink("ts3server://ts.avalon-network.net");
		
		if(event.getSource() == Interface.getInstance().getAvalon())
			openLink("https://avalon-network.net");
		
		// Settings
		
		//TODO: Add sound & settings
		
		if(event.getSource() == Interface.getInstance().getReduce())
			Interface.getInstance().setState(JFrame.ICONIFIED);
		
		if(event.getSource() == Interface.getInstance().getClose())
			System.exit(0);
		
		if(event.getSource() == Interface.getInstance().getPlay()) {
			try {
				
				Launcher.auth(Interface.getInstance().getUsername().getText(), Interface.getInstance().getPassword().getText());
				Launcher.update();
				Launcher.launch(Interface.getInstance().getUsername().getText());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void openLink(String url) {
		try {
			
			Launcher.getLogger().info("Opening Link : " + url);
			
			Desktop.getDesktop().browse(new URI(url));
			
			Launcher.getLogger().info("Opened Link : " + url);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

}
