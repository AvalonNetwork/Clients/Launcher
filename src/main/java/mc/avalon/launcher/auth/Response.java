package mc.avalon.launcher.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Response {

	private boolean authorized; 
	
}
