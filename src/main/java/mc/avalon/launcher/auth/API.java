package mc.avalon.launcher.auth;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

public class API {
	
	@Getter private static API instance;
	
	@Getter @Setter private Response response;
	@Getter @Setter private String Endpoint = "https://avalon-network.net/api/auth/login/";
	
	public API() {
		instance = this;
	}
	
	@SuppressWarnings("deprecation")
	public void call(String name, String password) throws Exception {
		
		Gson gson = new Gson();
		
		/**
		 * Endpoint
		 */
		
		this.setEndpoint(this.getEndpoint() + name + "/" + password);
		
		/**
		 * Create Response
		 */
		
		String response = readUrl("https://www.avalon-network.net/api/auth/login/Nezkaa/nezkaatest");
		this.setResponse(gson.fromJson(response, Response.class));
	}
	
	private static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
	

}
