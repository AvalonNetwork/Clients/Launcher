package mc.avalon.launcher.utils;

import lombok.Getter;

public class Informations {

	/**
	 * Launcher Informations
	 */
	
	@Getter private static String name = "Avalon";
	
	@Getter private static String authURL = "http://localhost:8080/";
	@Getter private static String updateURL = "http://update.avalon-network.net";
	
	/**
	 * Launcher Dimensions
	 */
	
	@Getter private static Integer width  = 673; // 873
	@Getter private static Integer height = 800; // 1000
	
	/**
	 * Launcher Versions
	 */
	
	@Getter private static String version = "Beta";
	@Getter private static String completeVersion = "1.0.0-BETA";
	
	/**
	 * Launcher Utilities
	 */
	
	@Getter private static String texturePath = "/mc/avalon/launcher/textures/";
}
