package mc.avalon.launcher.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Background extends JPanel {
	
	
	/**
	 * Background Image
	 */
	private Image background;
	
	/**
	 * Constructor 
	 * @param background	Name of the ressource.
	 * @param width			Width of the Launcher or the Background  Width's
	 * @param height		Height of the Launcher or the Background Height's
	 */
	
	public Background(Image image, int width, int height) {
		
		this.background = makeColorTransparent(image, Color.WHITE);
		
		Dimension size = new Dimension(width, height);
		
		this.setOpaque(false);
		this.setSize(size);
		this.setPreferredSize(size);
		this.setMaximumSize(size);
		this.setMinimumSize(size);
		this.setLayout(null);
	}
	
	/**
	 * Draw the background.
	 */
	
	public void paintComponent(Graphics graphics) {
		graphics.drawImage(this.background, 0, 0, this.getWidth(), this.getHeight(), this);
	}
	
	  public static Image makeColorTransparent(Image im, final Color color) {
		    ImageFilter filter = new RGBImageFilter() {
		      // the color we are looking for... Alpha bits are set to opaque
		      public int markerRGB = color.getRGB() | 0xFF000000;

		      @Override
		      public final int filterRGB(int x, int y, int rgb) {
		        if ((rgb | 0xFF000000) == markerRGB) {
		          // Mark the alpha bits as zero - transparent
		          return 0x00FFFFFF & rgb;
		        } else {
		          // nothing to do
		          return rgb;
		        }
		      }
		    };

		    ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
		    return Toolkit.getDefaultToolkit().createImage(ip);
		  }
		    

}
