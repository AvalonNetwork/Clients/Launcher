package mc.avalon.launcher.utils;

import java.io.File;

public class Directory {
	
	/**
	 * Return {@link File} where Launcher need to be installed.
	 * @return	File
	 */
	
	public File get() {
		String os = System.getProperty("os.name").toLowerCase();
		
		String name = Informations.getName();
		String version = Informations.getVersion();
		
		if (os.contains("win")) return new File(System.getProperty("user.home") + "\\AppData\\Roaming\\" + name + "/" + version);
		if (os.contains("mac")) return new File(System.getProperty("user.home") + "/Library/Application Support/" + name + "/" + version);

		return new File(System.getProperty("user.home") + "/" + name + "/" + version);
	}
}
