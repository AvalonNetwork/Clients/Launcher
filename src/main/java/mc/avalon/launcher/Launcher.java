package mc.avalon.launcher;

import java.awt.Frame;

import javax.swing.JOptionPane;

import com.neziaa.swinger.Swinger;
import com.neziaa.updater.BarAPI;
import com.neziaa.updater.Updaterz;

import fr.litarvan.openauth.AuthPoints;
import fr.litarvan.openauth.AuthenticationException;
import fr.litarvan.openauth.Authenticator;
import fr.litarvan.openauth.model.AuthProfile;
import fr.litarvan.openauth.model.response.AuthResponse;
import fr.theshark34.openlauncherlib.LaunchException;
import fr.theshark34.openlauncherlib.external.ExternalLaunchProfile;
import fr.theshark34.openlauncherlib.external.ExternalLauncher;
import fr.theshark34.openlauncherlib.minecraft.AuthInfos;
import fr.theshark34.openlauncherlib.minecraft.GameFolder;
import fr.theshark34.openlauncherlib.minecraft.GameInfos;
import fr.theshark34.openlauncherlib.minecraft.GameType;
import fr.theshark34.openlauncherlib.minecraft.GameVersion;
import fr.theshark34.openlauncherlib.minecraft.MinecraftLauncher;
import lombok.Getter;
import mc.avalon.launcher.auth.API;
import mc.avalon.launcher.auth.Response;
import mc.avalon.launcher.interfaces.Interface;
import mc.avalon.launcher.utils.Directory;
import mc.avalon.launcher.utils.Informations;
import net.wytrem.wylog.BasicLogger;
import net.wytrem.wylog.LoggerFactory;

public class Launcher {

	/**
	 * Logger 
	 */
	
	@Getter public static BasicLogger logger;
	
	/**
	 * Currently debugging?
	 */
	
	@Getter public static boolean debug;

	public static void main(String[] args) {
		
		/**
		 * Logger
		 */
		
		logger = LoggerFactory.getLogger(Informations.getName());
		
		/**
		 * Swinger
		 */

		Swinger.setSystemLookNFeel();
		Swinger.setResourcePath(Informations.getTexturePath());
		
		/**
		 * Currently debugging?
		 */
		
		for(String argument: args)
			if(argument == "debug")
				debug = true;
		
		/**
		 * Instancing Interface
		 */
		
		new Interface();
//		new SoundManager();

	}
	
	public static void launch(String username) {
		
    	if(!API.getInstance().getResponse().isAuthorized() || API.getInstance().getResponse() == null) {
			JOptionPane.showMessageDialog(new Frame(),
					"Vous n'�tes pas autorisez � lancer le launcher.", "Erreur", 1);
			return;
		}
		
		GameInfos infos = new GameInfos("Avalon", new Directory().get(), new GameVersion("1.7.10", GameType.V1_7_10), null);
		AuthInfos authInfos = new AuthInfos(username, null, null);
		
		try {
			
			ExternalLaunchProfile profile = MinecraftLauncher.createExternalProfile(infos, GameFolder.BASIC, authInfos);
			ExternalLauncher launcher = new ExternalLauncher(profile);
				
			launcher.launch();

			
		} catch (LaunchException e) {
			e.printStackTrace();
		}
		
		System.exit(0);
		
	}
	
    public static void update() throws Exception {
    	if(!API.getInstance().getResponse().isAuthorized() || API.getInstance().getResponse() == null) {
    		return;
    	}
    	
        Updaterz updater = new Updaterz("http://update.avalon-network.net", new Directory().get());
        
        Thread thread = new Thread(){
            private int value;
            private int maximum;

            @Override
            public void run() {
                while (!this.isInterrupted()) {
                    if (BarAPI.getNumberOfFileToDownload() == 0) {
                        continue;
                    }
                    
                    this.value = (int)(BarAPI.getNumberOfTotalDownloadedBytes() / 1000);
                    this.maximum = (int)(BarAPI.getNumberOfTotalBytesToDownload() / 1000);
                    Interface.getInstance().getBar().setMaximum(maximum);
                    Interface.getInstance().getBar().setValue(value);
                }
            }
        };
        
        thread.start();
        updater.start();
        thread.interrupt();
    }
	
	public static void auth(String username, String password) throws AuthenticationException {
		
		/**
		 * API
		 */
		
		API api = new API();
		
		try {
			
			api.call(username, password);
			
		} catch (Exception e) {
			
			getLogger().error("Error", e);
			
		}
		
		/**
		 * Response checks
		 */
		
		Response response = api.getResponse();
		
		if(response == null) {
			JOptionPane.showMessageDialog(new Frame(),
					"Probl�me avec les serveurs d'authentification, si le probl�me persiste contactez un admin.", "Erreur", 1);
			return;
		}
	}
	


}
