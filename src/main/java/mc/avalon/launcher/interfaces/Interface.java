package mc.avalon.launcher.interfaces;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.neziaa.swinger.Swinger;
import com.neziaa.swinger.colored.ColoredBar;
import com.neziaa.swinger.textured.TexturedButton;

import lombok.Getter;
import mc.avalon.launcher.Launcher;
import mc.avalon.launcher.listeners.InterfaceListeners;
import mc.avalon.launcher.utils.Background;
import mc.avalon.launcher.utils.Informations;
import mc.avalon.launcher.utils.MouseManager;
import net.wytrem.wylog.BasicLogger;

@Getter @SuppressWarnings("serial")
public class Interface extends JFrame {
	
	/**
	 * Instance
	 */
	
	@Getter private static Interface instance;
	
	/**
	 * Logger
	 */
	
	private BasicLogger logger;
	
	/**
	 * Buttons
	 */
	
	private TexturedButton youtube;
	private TexturedButton discord;
	private TexturedButton twitter;
	private TexturedButton teamspeak;
	private TexturedButton avalon;
	
	private TexturedButton sound;
	private TexturedButton options;
	private TexturedButton reduce;
	private TexturedButton close;
	
	private TexturedButton play;
	
	/**
	 * TextBox
	 */
	
	private JTextField username;
	private JPasswordField password;
	
	/**
	 * ProgressBar
	 */
	
	private ColoredBar bar;
	
	public Interface() {
		
		/**
		 * Instance
		 */
		
		instance = this;
		
		/**
		 * Logger
		 */
		
		logger = Launcher.getLogger();
		
		/**
		 * Display
		 */
		
		this.setTitle(Informations.getName());
		this.setSize(Informations.getWidth(), Informations.getHeight());
		this.setUndecorated(true);
		this.setBackground(Swinger.TRANSPARENT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setLocationRelativeTo(null);
		
		logger.info("Ready to displaying.");
		
		/**
		 * Setup favicon
		 */
		
		this.setIconImage(Swinger.getResource("favicon.png"));
		logger.info("Favicon setup.");
		
		/**
		 * Mouse
		 */
		
		MouseManager mouseManager = new MouseManager(this);
		this.addMouseListener(mouseManager);
		this.addMouseMotionListener(mouseManager);
		logger.info("MouseManager loaded.");
		
		/**
		 * Background
		 */
		
		Background background = new Background(Swinger.getResource("background.png"), Informations.getWidth(), Informations.getHeight());
		this.setContentPane(background);
		logger.info("Background drawed.");
		
		/**
		 * Foreground
		 */
		
		TexturedButton info = new TexturedButton(Swinger.getResource("informations.png"), Swinger.getResource("informations.png"));
		info.setBounds(245, 375, 325, 115);
		
		/**
		 * @Buttons 
		 *  Socials
		 */
		
		youtube = new TexturedButton(Swinger.getResource("youtube.png"), Swinger.getResource("youtube_hover.png"));
		youtube.setBounds(245, 280, 50, 50);
		
		twitter = new TexturedButton(Swinger.getResource("twitter.png"), Swinger.getResource("twitter_hover.png"));
		twitter.setBounds(315, 270, 50, 50);
		
		discord = new TexturedButton(Swinger.getResource("discord.png"), Swinger.getResource("discord_hover.png"));
		discord.setBounds(385, 275, 50, 50);
		
		teamspeak = new TexturedButton(Swinger.getResource("teamspeak.png"), Swinger.getResource("teamspeak_hover.png"));
		teamspeak.setBounds(455, 267, 50, 50);
		
		avalon = new TexturedButton(Swinger.getResource("avalon.png"), Swinger.getResource("avalon_hover.png"));
		avalon.setBounds(520, 275, 50, 50);

		
		/**
		 * @Buttons 
		 *  Settings
		 */
		
		sound = new TexturedButton(Swinger.getResource("sound_on.png"), Swinger.getResource("sound_off.png"));
		sound.setBounds(350, 75, 30, 30);
		
		options = new TexturedButton(Swinger.getResource("options.png"), Swinger.getResource("options_hover.png"));
		options.setBounds(382, 75, 30, 30);
		
		reduce = new TexturedButton(Swinger.getResource("reduce.png"), Swinger.getResource("reduce_hover.png"));
		reduce.setBounds(414, 90, 20, 10);
		
		close = new TexturedButton(Swinger.getResource("close.png"), Swinger.getResource("close_hover.png"));
		close.setBounds(440, 85, 20, 20);
		
		/**
		 * @Buttons 
		 *  Others
		 */
		
		play = new TexturedButton(Swinger.getResource("play.png"), Swinger.getResource("play_hover.png"));
		play.setBounds(240, Informations.getHeight() - 135, 180, 60);
		
		/**
		 * @TextBOX
		 */
		
		this.username = new JTextField();
		username.setFont(new Font("Montserrat", 0, 25));
        username.setCaretColor(Color.BLACK);
        username.setBorder(null);
        username.setBackground(new Color(0, 0, 0, 0));
        username.setForeground(Color.BLACK);
        username.setOpaque(false);
        username.setBounds(320, 370, 250, 50);
		
        
		this.password = new JPasswordField();
        password.setCaretColor(Color.BLACK);
        password.setBorder(null);
        password.setBackground(new Color(0, 0, 0, 0));
        password.setForeground(Color.BLACK);
        password.setOpaque(false);
        password.setBounds(320, 420, 250, 50);
        
        /**
         * ProgressBar
         */
		
        bar = new ColoredBar(Swinger.getTransparentInstance(Color.BLACK, 100), Swinger.getTransparentInstance(new Color(40, 53, 147), 100));
        bar.setBounds(230, Informations.getHeight() - 72, 200, 10);
	    
        /**
         * @Buttons
         *  Adding - Socials
         */
        
        add(youtube);
        add(twitter);
        add(discord);
        add(teamspeak);
        add(avalon);
        
        /**
         * @Buttons 
         *  Adding - Settings
         */
        
        add(sound);
        add(options);
        add(reduce);
        add(close);
        
        /**
         * @Buttons
         *  Adding - Others
         */
        
		add(play);
		
		/**
		 * Others
		 */
		
        add(password);
        add(username);
		add(info);
		
		/**
		 * Setup Listeners
		 */
		
		youtube.addEventListener(new InterfaceListeners());
		twitter.addEventListener(new InterfaceListeners());
		discord.addEventListener(new InterfaceListeners());
		teamspeak.addEventListener(new InterfaceListeners());
		avalon.addEventListener(new InterfaceListeners());
		
		sound.addEventListener(new InterfaceListeners());
		options.addEventListener(new InterfaceListeners());
		reduce.addEventListener(new InterfaceListeners());
		close.addEventListener(new InterfaceListeners());
		
		play.addEventListener(new InterfaceListeners());
		
		/**
		 * Setup.
		 */
		
	    add(bar);
		this.setVisible(true);
	}

}
